import React from "react";
import styled from "styled-components";

const Contact = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 24px;
  font-weight: 400;
  letter-spacing: 0.3px;
  line-height: 1.5;
  margin-top: 80px;
  padding: 10px;
  @media (max-width: 768px) {
    text-align: center;
  }
`;
const Link = styled.a`
  color: #de3c4b;
  text-decoration: none;
  width: 100%;
  :hover {
    color: #de3c4b;
  }
`;

export default function ContactText() {
  return (
    <Contact>
      <div>Media enquiries:</div>
      <Link href="mailto:press@modularbank.co">press@modularbank.co</Link>
      <br />
      <div>Career questions:</div>
      <Link href="mailto:careers@modularbank.co">careers@modularbank.co</Link>
      <br />
      <div>Our offices:</div>
      <div>Tallinn, Estonia</div>
      <div>Vabaduse Workland</div>
      <div>Pärnu mnt 12, 10146</div>
      <br />
      <div>Berlin, Germany</div>
      <div>Bikini Berlin, Scaling Spaces, 2.OG</div>
      <div>Budapester Str. 46</div>
      <div>10787 Berlin</div>
    </Contact>
  );
}
