import React, { InputHTMLAttributes } from "react";
import styled from "styled-components";

const SubmitButton = styled.input`
  background-color: #de3c4b;
  border: 3px solid transparent;
  color: #fff;
  cursor: pointer;
  font-size: 17px;
  font-weight: 300;
  height: auto;
  margin-right: 15px;
  margin-top: 5px;
  margin-bottom: 35px;
  padding: 20px 26px;
  position: absolute;
  right: 0;
  bottom: 0;
  :active {
    outline: none;
  }
  :disabled {
    cursor: not-allowed;
    opacity: 0.5;
  }
  @media (max-width: 768px) {
    margin-bottom: 0px;
  }
`;

export default function CustomInputButton(
  props: InputHTMLAttributes<HTMLInputElement>
) {
  return <SubmitButton {...props} />;
}
