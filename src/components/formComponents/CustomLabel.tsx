import React, { ReactNode } from "react";
import styled from "styled-components";

interface CustomLabelProps {
  children: ReactNode;
}
const Label = styled.label`
  font-size: 16px;
  margin-bottom: 10px;
  vertical-align: super;
`;

export default function CustomLabel({ children }: CustomLabelProps) {
  return <Label>{children}</Label>;
}
