# Cotact Page App

A contact us page app for client to leave their information and send in their enquiries.

- live [Demo](https://modularbank.netlify.app/)

# Technologies

- Node.js v10.x or later
- npm v5.x or later
- git v2.14.1 or later

## Installation

- Git clone repo [Modularbank-Task](https://gitlab.com/Tobzzy/modularbank-task)

#### Pre-requisite

```Bash
cd modularbank-Task
```

- Use the package manager [yarn](https://yarnpkg.com/) to install app

```bash
npm install or yarn install
```

- To start client

```bash
npm start or yarn start
```

- Client will be live on [localhost](http://localhost:3000) port 3000

# Important Choices

## Packages / Libraires

### Typescript

- Static type checker, which eliminates javascript silly mistakes like `object can be null or undefined`
- Gives componets a great structure and its reliable, I love building applications with rest of mind. :-)

### styled-components

- It makes component less big.
- CSS functions easier to use with props.
- Possibilty to also write test for styles when neccesary.

### semantic-ui

- Not my ideal goto UI design but in this case its needed for the select dropdown to acheive the specific UI layout.

## Component Structure

- Splitting component - To make component readable and reusable.
- Custom hooks - To avoid calling too many useState in the component itself.
- Using functions - To have a function that does only one specific thing.

## Optimization Anlysis

### In this case the application is small so optinization wasnt really neccsary but I used the following tecniques to acheive the most basic ones needed:

- Splitting components.
- Using object destructuring while importing a specific component from a module/ library.
- Using React Lazy and React Suspense to load components for better user experience.

### Author

- Toyib Olamide Ahmed
- Website - [Portfolio](https://tobzzy.github.io/)
- Email - (ahmedtoyib1@gmail.com)
- Reach out for collaboration lets build something awesome together.

## License

[MIT](https://choosealicense.com/licenses/mit/)
