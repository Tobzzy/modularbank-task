import React, { ReactNode } from "react";
import styled from "styled-components";

interface CustomFormProps {
  children: ReactNode;
}

const Form = styled.form`
  font-weight: 400;
  margin-top: 15px;
  position: relative;
  text-align: start;
`;
export default function CustomForm({ children }: CustomFormProps) {
  return <Form>{children}</Form>;
}
