import React, { useState } from "react";
import { Dropdown } from "semantic-ui-react";
import styled from "styled-components";
import Form from "../components/formComponents/CustomForm";
import Input from "../components/formComponents/CustomInput";
import Button from "../components/formComponents/CustomInputButton";
import Label from "../components/formComponents/CustomLabel";
import Textarea from "../components/formComponents/CustomTextarea";
import { countryData } from "../data/countryData";
import { industryData, operationData } from "../data/formData";
import { validateEmail } from "../functions/validateEmail";
import { useForm } from "../hooks/useForm";
import { useSelect } from "../hooks/useSelect";
import CustomModal from "./CustomModal";
interface CustomDropdownProps {
  minheight: string;
}
const InputWrapper = styled.div`
  display: flex;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;
const InputDiv = styled.div`
  display: flex;
  flex-direction: column;
  padding: 12px 15px;
  width: 100%;
`;
const CheckboxWrapper = styled.div`
  min-height: 25px;
  position: relative;
  padding: 15px;
`;
const InputCheckbox = styled.input`
  height: 25px;
  margin-right: 12px;
  width: 25px;
`;
const Link = styled.a`
  color: #de3c4b;
  text-decoration: none;
  :hover {
    color: #de3c4b;
  }
`;
const CustomDropdown = styled(Dropdown)<CustomDropdownProps>`
  border-radius: 0 !important;
  border: none;
  .menu {
    min-height: ${({ minheight }) => (minheight ? minheight : "100%")};
    .item {
      border: none !important;
    }
    .selected.item {
      background-color: #22304f !important;
      .text {
        color: #fff;
      }
    }
  }
`;
export default function ContactForm() {
  const [checkNewsletter, setCheckNewsletter] = useState(false);
  const [checkPrivacy, setCheckPrivacy] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [formValues, handleFormChange] = useForm({
    firstname: "",
    lastname: "",
    email: "",
    jobTitle: "",
    company: "",
    inquiry: "",
  });
  const [selectValues, handleSelectChange] = useSelect({
    industry: "Banking",
    country: "N/A",
    operation: "",
  });
  let button = true;
  const validEmail = validateEmail(formValues.email);
  if (
    formValues.firstname !== "" &&
    formValues.email !== "" &&
    formValues.company !== "" &&
    selectValues.industry !== "" &&
    selectValues.country !== "" &&
    checkPrivacy !== false
  ) {
    button = false;
  }
  const onClick = () => {
    if (validEmail) {
      //  just a simple email validation in this case, at this point we can simple do anything with all the input values (i.e save to databse)
      setShowModal(true);
      setTimeout(() => {
        setShowModal(false);
      }, 3000);
    }
    // Todo: else statement to return set error for validation, but not neccesary for this task
  };

  return (
    <Form>
      <InputWrapper>
        <InputDiv>
          <Label>First name*</Label>
          <Input type="text" name="firstname" onChange={handleFormChange} />
        </InputDiv>
        <InputDiv>
          <Label>Last name</Label>
          <Input
            type="text"
            name="lastname"
            onChange={handleFormChange}
          ></Input>
        </InputDiv>
      </InputWrapper>
      <InputWrapper>
        <InputDiv>
          <Label>Email*</Label>
          <Input type="email" name="email" onChange={handleFormChange} />
        </InputDiv>
        <InputDiv>
          <Label>Job title</Label>
          <Input
            type="text"
            name="jobTitle"
            onChange={handleFormChange}
          ></Input>
        </InputDiv>
      </InputWrapper>
      <br />
      <InputWrapper>
        <InputDiv>
          <Label>Company*</Label>
          <Input type="text" name="company" onChange={handleFormChange} />
        </InputDiv>
        <InputDiv>
          <Label>Industry*</Label>
          <CustomDropdown
            defaultValue="Banking"
            name="industry"
            selection
            options={industryData}
            minheight="250px"
            onChange={handleSelectChange}
          />
        </InputDiv>
      </InputWrapper>
      <InputWrapper>
        <InputDiv>
          <Label>Country*</Label>
          <CustomDropdown
            defaultValue="N/A"
            name="country"
            search
            selection
            options={countryData}
            minheight="300px"
            onChange={handleSelectChange}
          />
        </InputDiv>
        <InputDiv>
          <Label>Operating geography</Label>
          <CustomDropdown
            defaultValue="N/A"
            name="operation"
            selection
            options={operationData}
            onChange={handleSelectChange}
          />
        </InputDiv>
      </InputWrapper>
      <br />
      <InputDiv>
        <Label>What would you like to talk about?</Label>
        <Textarea name="inquiry" onChange={handleFormChange}></Textarea>
      </InputDiv>
      <InputWrapper>
        <CheckboxWrapper>
          <div>
            <InputCheckbox
              type="checkbox"
              defaultChecked={checkPrivacy}
              onChange={() => setCheckPrivacy(!checkPrivacy)}
            />
            <Label>
              By submitting this form I accept{" "}
              <Link
                href="https://www.modularbank.co/privacy-policy/"
                target="blank"
              >
                privacy policy and cookie policy
              </Link>
              {"."}
            </Label>
          </div>
          <br />
          <div>
            <InputCheckbox
              type="checkbox"
              defaultChecked={checkNewsletter}
              onChange={() => setCheckNewsletter(!checkNewsletter)}
            ></InputCheckbox>
            <Label>I would like to receive your newsletter.</Label>
          </div>
        </CheckboxWrapper>
        <div>
          <Button
            type="submit"
            value="Send"
            disabled={button}
            onClick={onClick}
          ></Button>
        </div>
      </InputWrapper>
      {showModal && <CustomModal />}
    </Form>
  );
}
