import React, { lazy, Suspense } from "react";
import styled from "styled-components";

const ContactText = lazy(() => import("./ContactText"));
const ContactForm = lazy(() => import("./ContactForm"));

const LoadingOverlay = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 40px;
`;
const Main = styled.div`
  display: grid;
  grid-template-columns: 40% 60%;
  margin: auto;
  max-width: 1250px;
  width: 100%;
  @media (max-width: 768px) {
    grid-template-columns: 1fr;
    grid-template-rows: -webkit-fit-content 1fr 1fr;
    grid-template-rows: fit-content 1fr 1fr;
  }
`;
const Title = styled.div`
  font-size: 45px;
  font-weight: 700;
  padding: 15px;
`;
const Form = styled.div`
  margin-top: 15px;
  margin-bottom: 15px;
`;

export default function App() {
  return (
    <Suspense fallback={<LoadingOverlay>Loading...</LoadingOverlay>}>
      <Main>
        <ContactText />
        <Form>
          <Title>Contact us</Title>
          <ContactForm />
        </Form>
      </Main>
    </Suspense>
  );
}
