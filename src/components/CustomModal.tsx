import React from "react";
import styled from "styled-components";

const ModalWrapper = styled.div`
  align-items: center;
  bottom: 0;
  display: flex;
  justify-content: center;
  left: 0;
  position: fixed;
  right: 0;
  top: 0;
`;
const Modal = styled.div`
  background-color: #22304f;
  border-radius: 16px;
  display: flex;
  flex-direction: column;
  padding: 16px;
  position: relative;
  width: 100%;
  height: 100%;
  z-index: 1;
`;
const Content = styled.div`
  left: 50%;
  position: absolute;
  text-align: center;
  top: 50%;
  transform: translate(-50%, -50%);
`;
const Title = styled.h1`
  font-size: 30px;
  font-weight: 700;
`;
const Text = styled.p`
  font-size: 22px;
  font-weight: 300;
`;

export default function CustomModal() {
  return (
    <ModalWrapper>
      <Modal>
        <Content>
          <Title>All good!</Title>
          <Text>
            Thank you for your interest. We will contact you in 1-2 working
            days.
          </Text>
        </Content>
      </Modal>
    </ModalWrapper>
  );
}
