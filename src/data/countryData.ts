interface CountryDataProps {
  value: string;
  flag?: string;
  text: string;
}

export const countryData: CountryDataProps[] = [
  {
    value: "N/A",
    text: "N/A",
  },
  {
    value: "ai",
    flag: "ai",
    text: "Ascension Island",
  },
  {
    value: "ad",
    flag: "ad",
    text: "Andorra",
  },
  {
    value: "ae",
    flag: "ae",
    text: "United Arab Emirates",
  },
  {
    value: "af",
    flag: "af",
    text: "Afghanistan",
  },
  {
    value: "ag",
    flag: "ag",
    text: "Antigua & Barbuda",
  },
  {
    value: "al",
    flag: "al",
    text: "Albania",
  },
  {
    value: "am",
    flag: "am",
    text: "Armenia",
  },
  {
    value: "ao",
    flag: "ao",
    text: "Angola",
  },
  {
    value: "ar",
    flag: "ar",
    text: "Argentina",
  },
  {
    value: "as",
    flag: "as",
    text: "American Samoa",
  },
  {
    value: "at",
    flag: "at",
    text: "Austria",
  },
  {
    value: "au",
    flag: "au",
    text: "Australia",
  },
  {
    value: "aw",
    flag: "aw",
    text: "Aruba",
  },
  {
    value: "ax",
    flag: "ax",
    text: "Åland Islands",
  },
  {
    value: "az",
    flag: "az",
    text: "Azerbaijan",
  },
  {
    value: "ba",
    flag: "ba",
    text: "Bosnia & Herzegovina",
  },
  {
    value: "bb",
    flag: "bb",
    text: "Barbados",
  },
  {
    value: "bd",
    flag: "bd",
    text: "Bangladesh",
  },
  {
    value: "be",
    flag: "be",
    text: "Belgium",
  },
  {
    value: "bf",
    flag: "bf",
    text: "Burkina Faso",
  },
  {
    value: "bg",
    flag: "bg",
    text: "Bulgaria",
  },
  {
    value: "bh",
    flag: "bh",
    text: "Bahrain",
  },
  {
    value: "bi",
    flag: "bi",
    text: "Burundi",
  },
  {
    value: "bj",
    flag: "bj",
    text: "Benin",
  },
  {
    value: "bm",
    flag: "bm",
    text: "Bermuda",
  },
  {
    value: "bn",
    flag: "bn",
    text: "Brunei",
  },
  {
    value: "bo",
    flag: "bo",
    text: "Bolivia",
  },
  {
    value: "br",
    flag: "br",
    text: "Brazil",
  },
  {
    value: "bs",
    flag: "bs",
    text: "Bahamas",
  },
  {
    value: "bt",
    flag: "bt",
    text: "Bhutan",
  },
  {
    value: "bv",
    flag: "bv",
    text: "Bouvet Island",
  },
  {
    value: "bw",
    flag: "bw",
    text: "Botswana",
  },
  {
    value: "by",
    flag: "by",
    text: "Belarus",
  },
  {
    value: "bz",
    flag: "bz",
    text: "Belize",
  },
  {
    value: "ca",
    flag: "ca",
    text: "Canada",
  },
  {
    value: "cc",
    flag: "cc",
    text: "Cocos (Keeling) Islands",
  },
  {
    value: "cd",
    flag: "cd",
    text: "Congo - Kinshasa",
  },
  {
    value: "cf",
    flag: "cf",
    text: "Central African Republic",
  },
  {
    value: "cg",
    flag: "cg",
    text: "Congo - Brazzaville",
  },
  {
    value: "ch",
    flag: "ch",
    text: "Switzerland",
  },
  {
    value: "ci",
    flag: "ci",
    text: "Côte d’Ivoire",
  },
  {
    value: "ck",
    flag: "ck",
    text: "Cook Islands",
  },
  {
    value: "cl",
    flag: "cl",
    text: "Chile",
  },
  {
    value: "cm",
    flag: "cm",
    text: "Cameroon",
  },
  {
    value: "cn",
    flag: "cn",
    text: "China",
  },
  {
    value: "co",
    flag: "co",
    text: "Colombia",
  },
  {
    value: "cr",
    flag: "cr",
    text: "Costa Rica",
  },
  {
    value: "cu",
    flag: "cu",
    text: "Cuba",
  },
  {
    value: "cv",
    flag: "cv",
    text: "Cape Verde",
  },
  {
    value: "cx",
    flag: "cx",
    text: "Christmas Island",
  },
  {
    value: "cy",
    flag: "cy",
    text: "Cyprus",
  },
  {
    value: "cz",
    flag: "cz",
    text: "Czechia",
  },
  {
    value: "de",
    flag: "de",
    text: "Germany",
  },
  {
    value: "dj",
    flag: "dj",
    text: "Djibouti",
  },
  {
    value: "dk",
    flag: "dk",
    text: "Denmark",
  },
  {
    value: "dm",
    flag: "dm",
    text: "Dominica",
  },
  {
    value: "do",
    flag: "do",
    text: "Dominican Republic",
  },
  {
    value: "dz",
    flag: "dz",
    text: "Algeria",
  },
  {
    value: "ec",
    flag: "ec",
    text: "Ecuador",
  },
  {
    value: "ee",
    flag: "ee",
    text: "Estonia",
  },
  {
    value: "eg",
    flag: "eg",
    text: "Egypt",
  },
  {
    value: "eh",
    flag: "eh",
    text: "Western Sahara",
  },
  {
    value: "er",
    flag: "er",
    text: "Eritrea",
  },
  {
    value: "es",
    flag: "es",
    text: "Spain",
  },
  {
    value: "et",
    flag: "et",
    text: "Ethiopia",
  },
  {
    value: "eu",
    flag: "eu",
    text: "European Union",
  },
  {
    value: "fi",
    flag: "fi",
    text: "Finland",
  },
  {
    value: "fj",
    flag: "fj",
    text: "Fiji",
  },
  {
    value: "fk",
    flag: "fk",
    text: "Falkland Islands",
  },
  {
    value: "fm",
    flag: "fm",
    text: "Micronesia",
  },
  {
    value: "fo",
    flag: "fo",
    text: "Faroe Islands",
  },
  {
    value: "fr",
    flag: "fr",
    text: "France",
  },
  {
    value: "ga",
    flag: "ga",
    text: "Gabon",
  },
  {
    value: "gb",
    flag: "gb",
    text: "United Kingdom",
  },
  {
    value: "gd",
    flag: "gd",
    text: "Grenada",
  },
  {
    value: "ge",
    flag: "ge",
    text: "Georgia",
  },
  {
    value: "gf",
    flag: "gf",
    text: "French Guiana",
  },
  {
    value: "gh",
    flag: "gh",
    text: "Ghana",
  },
  {
    value: "gl",
    flag: "gl",
    text: "Greenland",
  },
  {
    value: "gm",
    flag: "gm",
    text: "Gambia",
  },
  {
    value: "gn",
    flag: "gn",
    text: "Guinea",
  },
  {
    value: "gp",
    flag: "gp",
    text: "Guadeloupe",
  },
  {
    value: "gq",
    flag: "gq",
    text: "Equatorial Guinea",
  },
  {
    value: "gr",
    flag: "gr",
    text: "Greece",
  },
  {
    value: "gs",
    flag: "gs",
    text: "South Georgia & South Sandwich Islands",
  },
  {
    value: "gt",
    flag: "gt",
    text: "Guatemala",
  },
  {
    value: "gu",
    flag: "gu",
    text: "Guam",
  },
  {
    value: "gy",
    flag: "gy",
    text: "Guyana",
  },
  {
    value: "hk",
    flag: "hk",
    text: "Hong Kong SAR China",
  },
  {
    value: "hm",
    flag: "hm",
    text: "Heard & McDonald Islands",
  },
  {
    value: "hn",
    flag: "hn",
    text: "Honduras",
  },
  {
    value: "hr",
    flag: "hr",
    text: "Croatia",
  },
  {
    value: "ht",
    flag: "ht",
    text: "Haiti",
  },
  {
    value: "hu",
    flag: "hu",
    text: "Hungary",
  },
  {
    value: "id",
    flag: "id",
    text: "Indonesia",
  },
  {
    value: "ie",
    flag: "ie",
    text: "Ireland",
  },
  {
    value: "il,",
    flag: "il",
    text: "Israel",
  },
  {
    value: "in",
    flag: "in ",
    text: "India",
  },
  {
    value: "io",
    flag: "io",
    text: "British Indian Ocean Territory",
  },
  {
    value: "iq",
    flag: "iq",
    text: "Iraq",
  },
  {
    value: "ir",
    flag: "ir",
    text: "Iran",
  },
  {
    value: "is",
    flag: "is",
    text: "Iceland",
  },
  {
    value: "it",
    flag: "it",
    text: "Italy",
  },
  {
    value: "jm",
    flag: "jm",
    text: "Jamaica",
  },
  {
    value: "jo",
    flag: "jo",
    text: "Jordan",
  },
  {
    value: "jp",
    flag: "jp",
    text: "Japan",
  },
  {
    value: "ke",
    flag: "ke",
    text: "Kenya",
  },
  {
    value: "kg",
    flag: "kg",
    text: "Kyrgyzstan",
  },
  {
    value: "kh",
    flag: "kh",
    text: "Cambodia",
  },
  {
    value: "ki",
    flag: "ki",
    text: "Kiribati",
  },
  {
    value: "km",
    flag: "km",
    text: "Comoros",
  },
  {
    value: "kn",
    flag: "kn",
    text: "St. Kitts & Nevis",
  },
  {
    value: "kp",
    flag: "kp",
    text: "North Korea",
  },
  {
    value: "kr",
    flag: "kr",
    text: "South Korea",
  },
  {
    value: "kw",
    flag: "kw",
    text: "Kuwait",
  },
  {
    value: "ky",
    flag: "ky",
    text: "Cayman Islands",
  },
  {
    value: "kz",
    flag: "kz",
    text: "Kazakhstan",
  },
  {
    value: "la",
    flag: "la",
    text: "Laos",
  },
  {
    value: "lb",
    flag: "lb",
    text: "Lebanon",
  },
  {
    value: "lc",
    flag: "lc",
    text: "St. Lucia",
  },
  {
    value: "li",
    flag: "li",
    text: "Liechtenstein",
  },
  {
    value: "lk",
    flag: "lk",
    text: "Sri Lanka",
  },
  {
    value: "lr",
    flag: "lr",
    text: "Liberia",
  },
  {
    value: "ls",
    flag: "ls",
    text: "Lesotho",
  },
  {
    value: "lt",
    flag: "lt",
    text: "Lithuania",
  },
  {
    value: "lu",
    flag: "lu",
    text: "Luxembourg",
  },
  {
    value: "lv",
    flag: "lv",
    text: "Latvia",
  },
  {
    value: "ly",
    flag: "ly",
    text: "Libya",
  },
  {
    value: "ma",
    flag: "ma",
    text: "Morocco",
  },
  {
    value: "mc",
    flag: "mc",
    text: "Monaco",
  },
  {
    value: "md",
    flag: "md",
    text: "Moldova",
  },
  {
    value: "me",
    flag: "me",
    text: "Montenegro",
  },
  {
    value: "mg",
    flag: "mg",
    text: "Madagascar",
  },
  {
    value: "mh",
    flag: "mh",
    text: "Marshall Islands",
  },
  {
    value: "mk",
    flag: "mk",
    text: "Macedonia",
  },
  {
    value: "ml",
    flag: "ml",
    text: "Mali",
  },
  {
    value: "mm",
    flag: "mm",
    text: "Myanmar (Burma)",
  },
  {
    value: "mn",
    flag: "mn",
    text: "Mongolia",
  },
  {
    value: "mo",
    flag: "mo",
    text: "Macau SAR China",
  },
  {
    value: "mp",
    flag: "mp",
    text: "Northern Mariana Islands",
  },
  {
    value: "mq",
    flag: "mq",
    text: "Martinique",
  },
  {
    value: "mr",
    flag: "mr",
    text: "Mauritania",
  },
  {
    value: "ms",
    flag: "ms",
    text: "Montserrat",
  },
  {
    value: "mt",
    flag: "mt",
    text: "Malta",
  },
  {
    value: "mu",
    flag: "mu",
    text: "Mauritius",
  },
  {
    value: "mv",
    flag: "mv",
    text: "Maldives",
  },
  {
    value: "mw",
    flag: "mw",
    text: "Malawi",
  },
  {
    value: "mx",
    flag: "mx",
    text: "Mexico",
  },
  {
    value: "my",
    flag: "my",
    text: "Malaysia",
  },
  {
    value: "mz",
    flag: "mz",
    text: "Mozambique",
  },
  {
    value: "na",
    flag: "na",
    text: "Namibia",
  },
  {
    value: "nc",
    flag: "nc",
    text: "New Caledonia",
  },
  {
    value: "ne",
    flag: "ne",
    text: "Niger",
  },
  {
    value: "nf",
    flag: "nf",
    text: "Norfolk Island",
  },
  {
    value: "ng",
    flag: "ng",
    text: "Nigeria",
  },
  {
    value: "ni",
    flag: "ni",
    text: "Nicaragua",
  },
  {
    value: "nl",
    flag: "nl",
    text: "Netherlands",
  },
  {
    value: "no",
    flag: "no",
    text: "Norway",
  },
  {
    value: "np",
    flag: "np",
    text: "Nepal",
  },
  {
    value: "nr",
    flag: "nr",
    text: "Nauru",
  },
  {
    value: "nu",
    flag: "nu",
    text: "Niue",
  },
  {
    value: "nz",
    flag: "nz",
    text: "New Zealand",
  },
  {
    value: "om",
    flag: "om",
    text: "Oman",
  },
  {
    value: "pa",
    flag: "pa",
    text: "Panama",
  },
  {
    value: "pe",
    flag: "pe",
    text: "Peru",
  },
  {
    value: "pf",
    flag: "pf",
    text: "French Polynesia",
  },
  {
    value: "pg",
    flag: "pg",
    text: "Papua New Guinea",
  },
  {
    value: "ph",
    flag: "ph",
    text: "Philippines",
  },
  {
    value: "pk",
    flag: "pk",
    text: "Pakistan",
  },
  {
    value: "pl",
    flag: "pl",
    text: "Poland",
  },
  {
    value: "pm",
    flag: "pm",
    text: "St. Pierre & Miquelon",
  },
  {
    value: "pn",
    flag: "pn",
    text: "Pitcairn Islands",
  },
  {
    value: "pr",
    flag: "pr",
    text: "Puerto Rico",
  },
  {
    value: "ps",
    flag: "ps",
    text: "Palestinian Territories",
  },
  {
    value: "pt",
    flag: "pt",
    text: "Portugal",
  },
  {
    value: "pw",
    flag: "pw",
    text: "Palau",
  },
  {
    value: "py",
    flag: "py",
    text: "Paraguay",
  },
  {
    value: "qa",
    flag: "qa",
    text: "Qatar",
  },
  {
    value: "re",
    flag: "re",
    text: "Réunion",
  },
  {
    value: "ro",
    flag: "ro",
    text: "Romania",
  },
  {
    value: "rs",
    flag: "rs",
    text: "Serbia",
  },
  {
    value: "ru",
    flag: "ru",
    text: "Russia",
  },
  {
    value: "rw",
    flag: "rw",
    text: "Rwanda",
  },
  {
    value: "sa",
    flag: "sa",
    text: "Saudi Arabia",
  },
  {
    value: "sb",
    flag: "sb",
    text: "Solomon Islands",
  },
  {
    value: "sc",
    flag: "sc",
    text: "Seychelles",
  },
  {
    value: "sd",
    flag: "sd",
    text: "Sudan",
  },
  {
    value: "se",
    flag: "se",
    text: "Sweden",
  },
  {
    value: "sg",
    flag: "sg",
    text: "Singapore",
  },
  {
    value: "sh",
    flag: "sh",
    text: "St. Helena",
  },
  {
    value: "si",
    flag: "si",
    text: "Slovenia",
  },
  {
    value: "sj",
    flag: "sj",
    text: "Svalbard & Jan Mayen",
  },
  {
    value: "sk",
    flag: "sk",
    text: "Slovakia",
  },
  {
    value: "sl",
    flag: "sl",
    text: "Sierra Leone",
  },
  {
    value: "sm",
    flag: "sm",
    text: "San Marino",
  },
  {
    value: "sn",
    flag: "sn",
    text: "Senegal",
  },
  {
    value: "so",
    flag: "so",
    text: "Somalia",
  },
  {
    value: "sr",
    flag: "sr",
    text: "Suritext",
  },
  {
    value: "st",
    flag: "st",
    text: "São Tomé & Príncipe",
  },
  {
    value: "sv",
    flag: "sv",
    text: "El Salvador",
  },
  {
    value: "sy",
    flag: "sy",
    text: "Syria",
  },
  {
    value: "sz",
    flag: "sz",
    text: "Swaziland",
  },
  {
    value: "tc",
    flag: "tc",
    text: "Turks & Caicos Islands",
  },
  {
    value: "td",
    flag: "td",
    text: "Chad",
  },
  {
    value: "tf",
    flag: "tf",
    text: "French Southern Territories",
  },
  {
    value: "tg",
    flag: "tg",
    text: "Togo",
  },
  {
    value: "th",
    flag: "th",
    text: "Thailand",
  },
  {
    value: "tj",
    flag: "tj",
    text: "Tajikistan",
  },
  {
    value: "tk",
    flag: "tk",
    text: "Tokelau",
  },
  {
    value: "tl",
    flag: "tl",
    text: "Timor-Leste",
  },
  {
    value: "tm",
    flag: "tm",
    text: "Turkmenistan",
  },
  {
    value: "tn",
    flag: "tn",
    text: "Tunisia",
  },
  {
    value: "to",
    flag: "to",
    text: "Tonga",
  },
  {
    value: "tr",
    flag: "tr",
    text: "Turkey",
  },
  {
    value: "tt",
    flag: "tt",
    text: "Trinidad & Tobago",
  },
  {
    value: "tv",
    flag: "tv",
    text: "Tuvalu",
  },
  {
    value: "tw",
    flag: "tw",
    text: "Taiwan",
  },
  {
    value: "tz",
    flag: "tz",
    text: "Tanzania",
  },
  {
    value: "ua",
    flag: "ua",
    text: "Ukraine",
  },
  {
    value: "ug",
    flag: "ug",
    text: "Uganda",
  },
  {
    value: "um",
    flag: "um",
    text: "U.S. Outlying Islands",
  },
  {
    value: "un",
    flag: "un",
    text: "United Nations",
  },
  {
    value: "us",
    flag: "us",
    text: "United States",
  },
  {
    value: "uy",
    flag: "uy",
    text: "Uruguay",
  },
  {
    value: "uz",
    flag: "uz",
    text: "Uzbekistan",
  },
  {
    value: "va",
    flag: "va",
    text: "Vatican City",
  },
  {
    value: "vc",
    flag: "vc",
    text: "St. Vincent & Grenadines",
  },
  {
    value: "ve",
    flag: "ve",
    text: "Venezuela",
  },
  {
    value: "vg",
    flag: "vg",
    text: "British Virgin Islands",
  },
  {
    value: "vi",
    flag: "vi",
    text: "U.S. Virgin Islands",
  },
  {
    value: "vn",
    flag: "vn",
    text: "Vietnam",
  },
  {
    value: "vu",
    flag: "vu",
    text: "Vanuatu",
  },
  {
    value: "wf",
    flag: "wf",
    text: "Wallis & Futuna",
  },
  {
    value: "ws",
    flag: "ws",
    text: "Samoa",
  },
  {
    value: "ye",
    flag: "ye",
    text: "Yemen",
  },
  {
    value: "yt",
    flag: "yt",
    text: "Mayotte",
  },
  {
    value: "za",
    flag: "za",
    text: "South Africa",
  },
  {
    value: "zm",
    flag: "zm",
    text: "Zambia",
  },
  {
    value: "zw",
    flag: "zw",
    text: "Zimbabwe",
  },
];
