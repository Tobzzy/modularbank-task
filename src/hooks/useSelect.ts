import { useState } from "react";

export const useSelect = (initialValues: any) => {
  const [selectValues, setSelectValues] = useState(initialValues);

  return [
    selectValues,
    (e: any, data: { name: string; value: string }) => {
      setSelectValues({
        ...selectValues,
        [data.name]: data.value,
      });
    },
  ];
};
