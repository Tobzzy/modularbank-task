interface DataProps {
  value: string;
  text: string;
}

export const industryData: DataProps[] = [
  { value: "Banking", text: "Banking" },
  { value: "Automotive", text: "Automotive" },
  { value: "Consult", text: "Consult" },
  { value: "Finance", text: "Finance" },
  { value: "Healthcare", text: "Healthcare" },
  { value: "Media/Pr", text: "Media/Pr" },
  { value: "Retail", text: "Retail" },
  { value: "Technology", text: "Technology" },
  { value: "Telecommunication", text: "Telecommunication" },
  { value: "Other", text: "Other" },
];

export const operationData: DataProps[] = [
  { value: "N/A", text: "N/A" },
  { value: "National", text: "National" },
  { value: "Regional", text: "Regional" },
  { value: "Global", text: "Global" },
];
