import React, { InputHTMLAttributes } from "react";
import styled from "styled-components";

const Input = styled.input`
  border-radius: 0;
  border: none;
  color: #363636;
  outline: none;
  padding: 10px;
`;

export default function CustomInput(
  props: InputHTMLAttributes<HTMLInputElement>
) {
  return <Input {...props} />;
}
