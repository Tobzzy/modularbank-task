import React, { TextareaHTMLAttributes } from "react";
import styled from "styled-components";

const Textarea = styled.textarea`
  color: #363636;
  font-family: "CalibreWeb";
  max-height: 40em;
  min-height: 8em;
  outline: none;
  resize: none;
  width: 100%;
`;
export default function CustomTextarea(
  props: TextareaHTMLAttributes<HTMLTextAreaElement>
) {
  return <Textarea {...props} />;
}
